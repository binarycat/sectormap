{ pkgs ? import <nixpkgs> {}
, stdenv ? pkgs.stdenv
}:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [ gcc lua ];
}
