//! datastructrue for effeciently handling collisons in simulations with many objects.

pub struct SectorMap {
	span: u32,
	sectors: Vec<Vec<u32>>,
}

impl SectorMap {
	pub fn new(x: u32, y: u32) -> Box<SectorMap> {
		return Box::new(SectorMap{
			span: x,
			sectors: vec![Vec::new(); (x * y).try_into().unwrap()],
		});
	}
	pub fn clear(&mut self) {
		for sect in self.sectors.iter_mut() {
			sect.clear()
		}
	}
	pub fn add(&mut self, x: u32, y: u32, v: u32) {
		self.sectors[(x+self.span*y) as usize].push(v);
	}
	pub fn get(&self, x: u32, y: u32, i: u32) -> u32 {
		let sect: &Vec<u32> = &self.sectors[(x+self.span*y) as usize];
		if (i as usize) < sect.len() {
			return sect[i as usize];
		} else {
			return u32::MAX;
		}
	}
	pub fn len(&self, x: u32, y: u32) -> u32 {
		return self.sectors[(x+self.span*y) as usize].len() as u32;
	}
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
		let mut sm = SectorMap::new(10, 10);
		sm.add(1, 1, 33);
		assert_eq!(sm.get(1, 1, 0), 33);
		assert_eq!(sm.get(1, 1, 1), u32::MAX);
		sm.clear();
		assert_eq!(sm.get(1, 1, 0), u32::MAX);
    }
}

