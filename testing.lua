local sectormap = require "sectormap"

assert(type(sectormap) == "table")
assert(type(sectormap.new) == "function")
assert(type(sectormap.clear) == "function")
assert(type(sectormap.add) == "function")
assert(type(sectormap.get) == "function")
assert(type(sectormap.len) == "function")

local sm1 = sectormap.new(5, 10)
assert(debug.getmetatable(sm1).__index == sectormap)
sm1:add(0, 0, 333)
sm1:add(4, 9, 777)

assert(sectormap.get(sm1, 0, 0, 0) == 333)
assert(sectormap.get(sm1, 0, 0, 1) == sectormap.NONE)
assert(sm1:get(4, 9, 0) == 777)
sm1:clear()
assert(sm1:get(0, 0, 0) == sectormap.NONE)

print "testing garbage collection"
for _ = 1,10 do
	for _ = 1,100 do
		local sm = sectormap.new(10, 10)
	end
	collectgarbage("collect")
	print("memory used: ", collectgarbage("count"))
end
