RUST_SRC = src/lib.rs glue/sectormap_core.rs
CORE_LIB = target/debug/examples/libsectormap_core.a

.PHONY: test
test: sectormap.so
	export LD_LIBRARY_PATH=$PWD/target/debug/examples/
	lua testing.lua

.PHONY: build
build: sectormap.so

.PHONY: clean
clean:
	rm sectormap.so
	cargo clean

sectormap.o: glue/sectormap.c glue/sectormap_core.h $(CORE_LIB)
	cc -o sectormap.o -c glue/sectormap.c

sectormap.so: sectormap.o
	ld -shared -llua -lsectormap_core -Ltarget/debug/examples -o sectormap.so sectormap.o $(CORE_LIB)

$(CORE_LIB): $(RUST_SRC)
	cargo build --example sectormap_core

