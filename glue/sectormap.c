#include <stdint.h>
#include <lua.h>
#include <lauxlib.h>
#include "sectormap_core.h"

static int new(lua_State *L) {
  int n = lua_gettop(L);
  if (n != 2) luaL_error(L, "wrong number of arguments %d (expected 2)", n);
  uint32_t x = lua_tonumber(L, 1);
  uint32_t y = lua_tonumber(L, 2);
  SectorMap **sm = lua_newuserdata(L, sizeof (SectorMap*));
  *sm = sectormap_core_new(x, y);
  luaL_setmetatable(L, "SectorMap");
  return 1;
}

static int gc(lua_State *L) {
  SectorMap **sm = lua_touserdata(L, 1);
  if(*sm == NULL) luaL_error(L, "double-free of SectorMap");
  sectormap_core_free(*sm);
  *sm = NULL;
  return 0;
}

static int clear(lua_State *L) {
  SectorMap **sm = lua_touserdata(L, 1);
  sectormap_core_clear(*sm);
  return 0;
}

static int add(lua_State *L) {
  int n = lua_gettop(L);
  if (n != 4) luaL_error(L, "wrong number of arguments %d (expected 4e)", n);
  SectorMap **sm = lua_touserdata(L, 1);
  uint32_t x = lua_tonumber(L, 2);
  uint32_t y = lua_tonumber(L, 3);
  uint32_t v = lua_tonumber(L, 4);
  sectormap_core_add(*sm, x, y, v);
  return 0;
}

static int get(lua_State *L) {
  int n = lua_gettop(L);
  if (n != 4) luaL_error(L, "wrong number of arguments %d (expected 4)", n);
  SectorMap **sm = lua_touserdata(L, 1);
  uint32_t x = lua_tonumber(L, 2);
  uint32_t y = lua_tonumber(L, 3);
  uint32_t i = lua_tonumber(L, 4);
  lua_pushinteger(L, sectormap_core_get(*sm, x, y, i));
  return 1;
}

static int len(lua_State *L) {
  int n = lua_gettop(L);
  if (n != 3) luaL_error(L, "wrong number of arguments %d (expected 3)", n);
  SectorMap **sm = lua_touserdata(L, 1);
  uint32_t x = lua_tonumber(L, 2);
  uint32_t y = lua_tonumber(L, 3);
  lua_pushinteger(L, sectormap_core_len(*sm, x, y));
  return 1;
}

static luaL_Reg func_tbl[] = {
  { "new", new },
  { "clear", clear },
  { "add", add },
  { "get", get },
  { "len", len },
  { NULL, NULL }
};

int luaopen_sectormap(lua_State *L) {
  luaL_newlib(L, func_tbl);
  int mod = lua_gettop(L);
  lua_pushinteger(L, 0xFFFFFFFF);
  lua_setfield(L, -2, "NONE");
  if(luaL_newmetatable(L, "SectorMap")){
	int mt = lua_gettop(L);
	lua_pushcfunction(L, gc);
	lua_setfield(L, mt, "__gc");
	lua_pushvalue(L, mod);
	lua_setfield(L, mt, "__index");
  }
  lua_pop(L, 1);
  return 1;
}
