struct SectorMap;
typedef struct SectorMap SectorMap;

SectorMap* sectormap_core_new(uint32_t x, uint32_t y);
void sectormap_core_free(SectorMap* sm);
void sectormap_core_clear(SectorMap* sm);
void sectormap_core_add(SectorMap* sm, uint32_t x, uint32_t y, uint32_t v);
uint32_t sectormap_core_get(SectorMap* sm, uint32_t x, uint32_t y, uint32_t i);
uint32_t sectormap_core_len(SectorMap* sm, uint32_t x, uint32_t y);


