use sectormap::SectorMap;
use std::boxed::Box;

#[no_mangle]
pub unsafe extern "C" fn sectormap_core_new(x: u32, y: u32) -> *mut SectorMap {
	return Box::into_raw(SectorMap::new(x, y));
}

#[no_mangle]
pub unsafe extern "C" fn sectormap_core_free(sm: *mut SectorMap) {
	drop(Box::from_raw(sm));
}

#[no_mangle]
pub unsafe extern "C" fn sectormap_core_clear(sm: *mut SectorMap) {
	sm.as_mut().expect("invalid pointer").clear();
}

#[no_mangle]
pub unsafe extern "C" fn sectormap_core_add(sm: *mut SectorMap, x: u32, y: u32, v: u32) {
	sm.as_mut().expect("invalid pointer").add(x, y, v);
}

#[no_mangle]
pub unsafe extern "C" fn sectormap_core_get(sm: *mut SectorMap, x: u32, y: u32, i: u32) -> u32 {
	return sm.as_ref().expect("invalid pointer").get(x, y, i);
}

#[no_mangle]
pub unsafe extern "C" fn sectormap_core_len(sm: *mut SectorMap, x: u32, y: u32, i: u32) -> u32 {
	return sm.as_ref().expect("invalid pointer").len(x, y);
}


